# redditify

i honestly don't know

## Usage

The first argument provided should be some initial substring of either
`compress` or `decompress`. The program then operates on stdin and stdout, and
so to direct it from and to files would look something like the following.

```
redditify c <file_in >file_out.rdt
redditify d <file_in.rdt >file_out
```

## Running

To run without installing, pass the mode to `cargo run` after `--`.

```
cargo run -- c
cargo run -- d
```

## Building

```
cargo build --release
```

## Installing

After building, the binary will be located at `target/release/redditify`, and
may be relocated for installation anywhere.
