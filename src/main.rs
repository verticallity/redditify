// current exit codes bc too lazy to make an enum:
// 1 - can't get mode
// 2 - failed reading
// 3 - failed writing
// 4 - malformed input
// -1 - other
use std::num::NonZeroI32;
fn fatal(string: String, error_code: Option<NonZeroI32>) -> ! {
	eprintln!("Error: {}", string);
	let error_code = match error_code {
		Some(n) => n.get(),
		None => -1i32,
	};
	std::process::exit(error_code);
}

fn main() {
	let mut args = std::env::args_os();
	args.next();

	enum Mode {
		Compress,
		Decompress,
	}
	
	// this was a try block but going to roll it out instead
	// bc why do something under 200 lines on nightly
	let mode = match args.next() {
		None => fatal(String::from("please provide a mode ([c]ompress or [d]ecompress)"), NonZeroI32::new(1)),

		Some(m) => {
			match m.into_string() {
				Err(m) => fatal(format!("don't recognize mode `{:?}`, expected [c]ompress or [d]ecompress", m), NonZeroI32::new(1)),
				Ok(mut mode_arg) => {
					mode_arg.make_ascii_lowercase();

					if "compress".starts_with(&mode_arg) {
						Mode::Compress
					} else if "decompress".starts_with(&mode_arg) {
						Mode::Decompress
					} else {
						fatal(format!("don't recognize mode `{}`, expected [c]ompress or [d]ecompress", mode_arg), NonZeroI32::new(1))
					}
				}
			}
		},
	};

	/*
	use std::fs::File;

	let mut file_in = match args.next() {
		None => Err(String::from("please provide an input file"))?,
		Some(s) => match File::open(s) {
			Err(_) => Err(fmt!("could not open file `{}`", s))?,
			Ok(file) => file,
		},
	};

	// maybe eventually provide option to wait to open output file?
	// and just do writing all at once instead of buffered
	// in case you want to go back to the same file as was inputted

	let mut file_out = match args.next() {
		None => Err(String::from("please provide an output file"))?,
		Some(s) => match File::create(s) {
			Err(_) => Err(fmt!("could not create file `{}`", s))?,
			Ok(file) => file,
		},
	};
	*/
	// just going to use stdin and stdout for now
	// since i remembered you could direct files to there
	let mut file_in = std::io::stdin();
	let mut file_out = std::io::stdout();

	const BUFFER_LEN: usize = 256;
	let mut sinless_buffer = [0u8; BUFFER_LEN];
	let mut reddity_buffer = [0u8; BUFFER_LEN * 7];
	let reddit = "reddit".as_bytes();

	use std::io::{Read, Write};

	match mode {
		Mode::Compress => {
			// preprepare this buffer with 'reddit_reddit_reddit_' etc
			for i in 0..BUFFER_LEN {
				let off = 7 * i;
				reddity_buffer[off..off+6].copy_from_slice(reddit);
			}

			loop {
				let len = match file_in.read(&mut sinless_buffer[..]) {
					Ok(0) => break,
					Ok(l) => l,
					Err(_) => fatal(String::from("failed while reading from input file"), NonZeroI32::new(2)),
				};

				for i in 0..len {
					reddity_buffer[7 * i + 6] = sinless_buffer[i];
				}

				let written = file_out.write(&reddity_buffer[0..(7 * len)]);
				match written {
					Ok(l) if l == 7 * len => (),
					Ok(_) | Err(_) => fatal(String::from("failed while writing to output file"), NonZeroI32::new(3)),
				}
			}
		},
		Mode::Decompress => {
			loop {
				let len = match file_in.read(&mut reddity_buffer[..]) {
					Ok(0) => break,
					Ok(l) if l % 7 == 0 => l/7,
					Ok(_) => fatal(String::from("don't understand given input"), NonZeroI32::new(4)),
					Err(_) => fatal(String::from("failed while reading from input file"), NonZeroI32::new(2)),
				};

				for i in 0..len {
					let off = 7 * i;
					if &reddity_buffer[off..off+6] != reddit {
						fatal(String::from("don't understand given input"), NonZeroI32::new(4));
					}
				}
				for i in 0..len {
					sinless_buffer[i] = reddity_buffer[7 * i + 6];
				}

				let written = file_out.write(&sinless_buffer[0..len]);
				match written {
					Ok(l) if l == len => (),
					Ok(_) | Err(_) => fatal(String::from("failed while writing to output file"), NonZeroI32::new(3)),
				}
			}
		},
	}
}
